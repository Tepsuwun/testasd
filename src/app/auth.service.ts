import { Injectable } from '@angular/core';
import {HttpClient, HttpRequest, HttpHeaders, HttpProgressEvent} from '@angular/common/http';
// import { Api } from './api/api';

interface myData {
    success: any,
    message: string,
    data: any[],
    user_status: number,
    emp_id: string,
    emp_data: string,
    user: any[],
    admin: any[],
    status: string,
    users: any[],
    positions: any[],
    num_employee: number,
    num_superviser: number,
    num_manager: number,
    num_admin: number,
    num_all: number,
    num_allemp: number,
    num_positions: number,
    // token: string,
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedInStatus = false
    hostUrl = 'http://127.0.0.1:8000';
    // hostUrl = 'http://scodedev.com/laravel/public';
  constructor(private http: HttpClient,
              // private api: Api
                ) { }
    host_API() {
      // const API = 'http://localhost/scode_downTime/';
      const API = 'http://scodedev.com/laravel/';
      return API;
    }
    setLoggedIn(value: boolean) {
        this.loggedInStatus = value
    }

    get isLoggedIn() {
        return this.loggedInStatus
    }

    UpdateRegister(inputs) {
        return this.http.post<myData>(this.hostUrl + '/api/update_register', {
            inputs
        })
    }

    UploadImg(formData) {
        return this.http.post<myData>(this.hostUrl + '/api/UploadImg', formData)
    }

    loginUserApi( name, password ) {
        return this.http.post<myData>(this.hostUrl + '/api/login', {
            name,
            password
        })
    }
    test( name, password ) {
        return this.http.post<myData>(this.hostUrl + '/api/login', {
            name,
            password
        })
    }

    public getHeaders(): HttpHeaders {
        let Authorization = localStorage.getItem('token');
        let headers: HttpHeaders;
        headers = new HttpHeaders()
        // .set('Authorization', this.getAuthorization())
            .set('Authorization', 'Bearer ' + Authorization)
            .set('Content-Type', 'application/json');

        // headers.append('Access-Control-Allow-Headers', 'Content-Type');
        // // headers.append('Access-Control-Allow-Methods', 'GET');
        // headers.append('Access-Control-Allow-Origin', '*');

        return headers;
    }

    newPass (password, conpassword) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/newPassApi', {
            password, conpassword
        }, options)
    }

    add_star(emp_id, starRate, starRate2, starRate3, starRate4) {
        let headers: HttpHeaders;
        headers = this.getHeaders();
        let options: any;
        options = {
            headers: headers
        };
        return this.http.post<myData>(this.hostUrl + '/api/add_star', {
            emp_id, starRate, starRate2, starRate3, starRate4
        }, options)
    }
}
