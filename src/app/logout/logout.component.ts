import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-login',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(private Auth: AuthService,
              private router: Router) { }
  ngOnInit() {
      localStorage.clear();
      this.router.navigate(['/login']);
      console.log ('logout');
  }

}
