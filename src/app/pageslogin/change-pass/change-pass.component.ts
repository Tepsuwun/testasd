import {Component, OnDestroy, OnInit} from '@angular/core';

import { Router } from '@angular/router';
import {AuthService} from '../../auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

import 'style-loader!angular2-toaster/toaster.css';

@Component({
  selector: 'ngx-change-pass',
  templateUrl: './change-pass.component.html',
  styleUrls: ['./change-pass.component.scss']
})
export class ChangePassComponent implements OnInit {

    alluser = [];
    text = 'test';
    loginForm: FormGroup;
    isSubmitted: boolean = false;
    isConpass: boolean = false;
    number: string;
    constructor(private Auth: AuthService,
                private router: Router,
                private toasterService: ToasterService) { }

    config: ToasterConfig;

    position = 'toast-bottom-right';
    animationType = 'flyRight';
    title = 'HI there!';
    content = `I'm cool toaster!`;
    timeout = 5000;
    toastsLimit = 8;
    type = 'success';

    isNewestOnTop = true;
    isHideOnClick = true;
    isDuplicatesPrevented = false;
    isCloseButton = true;

    makeToast() {
        this.showToast(this.type, this.title, this.content);
    }


    private showToast(type: string, title: string, body: string) {
        this.config = new ToasterConfig({
            positionClass: this.position,
            timeout: this.timeout,
            newestOnTop: this.isNewestOnTop,
            tapToDismiss: this.isHideOnClick,
            preventDuplicates: this.isDuplicatesPrevented,
            animation: this.animationType,
            limit: this.toastsLimit,
        });
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: this.timeout,
            showCloseButton: this.isCloseButton,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this.toasterService.popAsync(toast);
    }

    ngOnInit() {
        this.number = localStorage.getItem('username');

        this.loginForm = new FormGroup({
            password: new FormControl('', [
                Validators.required,
                Validators.minLength(6),
                Validators.maxLength(20)
            ]),
            conpassword: new FormControl('', [
                Validators.required
            ])
        })

    }

    loginUser(event) {
        this.isSubmitted = true
        if (this.loginForm.valid === true) {
            this.showToast('default', 'แจ้งเตือน', 'กำลังเข้าสู่ระบบ');
            event.preventDefault()
            const target = event.target
            const password = target.querySelector('#password').value
            const conpassword = target.querySelector('#conpassword').value
            if (password === conpassword) {
                this.isConpass = false;
                this.showToast('success', 'แจ้งเตือน', 'ยืนยันรหัสผ่านถูกต้อง');
                console.log ('isConpass', this.isConpass);

                this.Auth.newPass(password, conpassword).subscribe(data => {
                    this.showToast('success', 'แจ้งเตือน', 'เปลี่ยนรหัสผ่านสำเร็จ');
                    localStorage.clear();
                    this.router.navigate(['/login']);
                })
            }else {
                this.showToast('error', 'แจ้งเตือน', 'ยืนยันรหัสผ่านไม่ถูกต้อง');
                this.isConpass = true;
                console.log ('isConpass', this.isConpass);
            }
        }else {
            this.showToast('error', 'ผิดพลาด', 'กรุณากรอกเบอร์โทรและรหัสผ่านให้ถูกต้อง');
        }
    }

}
