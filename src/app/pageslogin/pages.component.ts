import { Component } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';

@Component({
  selector: 'ngx-pages',
  template: `
    <fame-sample-layout>
      <router-outlet></router-outlet>
    </fame-sample-layout>
  `,
})
export class PagesComponent {

  menu = MENU_ITEMS;
}
